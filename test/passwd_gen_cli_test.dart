/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE file.
 */

import 'dart:convert';
import 'dart:io';

import 'package:passwd_gen_cli/daemon/index.dart';
import 'package:passwd_gen_cli/passwd_gen_cli.dart';
import 'package:passwd_gen_cli/texts_en.dart';
import 'package:test/test.dart';

const latin = r'[a-z]|[A-Z]|[0-9]|[\"£$%^&*()\-_=+[\]{};:@#~,.<>/?€\|]';
const french = r'[àâçéèêëîïôùûüÿæœ«»’]';
const german = r'[äöüß’]';
const italian = r'[àèéìòù’]';
const spanish = r'[ÁÉÍÑÓÚÜáéíñóúü¿¡]';

const katakana = r'[゠ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂ'
    'ッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮ'
    'ワヰヱヲンヴヵヶヷヸヹヺ・ーヽヾヿ]';

// ignore: lines_longer_than_80_chars
const emoji =
    r'[😀😁😂😃😄😅😆😇😈😉😊😋😌😍😎😏😐😑😒😓😔😕😖😗😘😙😚😛😜😝😞😟😠😡'
// ignore: lines_longer_than_80_chars
    '😢😣😤😥😦😧😨😩😪😫😬😭😮😯😰😱😲😳😴😵😶😷😸😹😺😻😼😽😾😿🙀🙁🙂🙃🙄🙅🙆🙇🙈'
    '🙉🙊🙋🙌🙍🙎🙏🙐]';

// https://github.com/dart-lang/pana/blob/master/test/bin_pana_test.dart
void main() {
  group('Command line tests', () {
    test('Test default', () async {
      var result = await Process.start('dart', ['run', 'passwd_gen_cli', '-o']);
      var exitCode = await result.exitCode;
      expect(exitCode, exitOk, reason: 'Expect succeed code 0');
      var out = await result.stdout.transform(utf8.decoder).toList();
      //print ('-> ${out.first}');
      expect(out.first.trim().length, defaultSize,
          reason: 'Expect default password length');
    });

    test('Test help', () async {
      var resultS = Process.runSync('dart',
          ['run', 'passwd_gen_cli', '-h']);
      var shortOut = resultS.stdout.toString().trim();
      var resultL = Process.runSync('dart',
          ['run', 'passwd_gen_cli', '-h']);
      var longOut = resultL.stdout.toString().trim();
      expect(shortOut == longOut, true,
          reason: 'Expect both result are equal.');
      expect(shortOut.contains(flags), true);
      expect(shortOut.contains(usage), true);
      expect(longOut.contains(options), true);
    });

    test('Test version', () async {
      var short = await Process.start('dart', ['run', 'passwd_gen_cli', '-v']);
      var long =
          await Process.start('dart', ['run', 'passwd_gen_cli', '--version']);
      var shortOut =
          (await short.stdout.transform(utf8.decoder).toList()).first;
      var longOut = (await long.stdout.transform(utf8.decoder).toList()).first;
      expect(shortOut == longOut, true,
          reason: 'Expect both result are equal.');
      expect(shortOut.contains(version), true);
    });

    test('Test license', () async {
      var short = await Process.start('dart', ['run', 'passwd_gen_cli', '-l']);
      var long =
          await Process.start('dart', ['run', 'passwd_gen_cli', '--license']);
      var shortOut =
          (await short.stdout.transform(utf8.decoder).toList()).first;
      var longOut = (await long.stdout.transform(utf8.decoder).toList()).first;
      expect(shortOut == longOut, true,
          reason: 'Expect both result are equal.');
      expect(shortOut.contains(license), true);
    });

    test('Test latin', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'latin']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(resultAsString.runes.length, defaultSize,
          reason: 'Expect default size');
    });

    test('Test french', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'french']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(resultAsString.contains(RegExp(french)), true,
          reason: 'Expect french characters');
      expect(resultAsString.runes.length, defaultSize,
          reason: 'Expect default size');
    });

    test('Test german', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'german']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(resultAsString.contains(RegExp(german)), true,
          reason: 'Expect german characters');
      expect(resultAsString.runes.length, defaultSize,
          reason: 'Expect default size');
    });

    test('Test italian', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'italian']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(resultAsString.contains(RegExp(italian)), true,
          reason: 'Expect italian characters');
      expect(resultAsString.runes.length, defaultSize,
          reason: 'Expect default size');
    });

    test('Test spanish', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'spanish']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(resultAsString.contains(RegExp(spanish)), true,
          reason: 'Expect spanish characters');
      expect(resultAsString.runes.length, defaultSize,
          reason: 'Expect default size');
    });

    test('Test eff', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'eff']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(
          resultAsString.split(String.fromCharCode(0x00A0)).length, defaultSize,
          reason: 'Expect default size');
    });

    test('Test size -s', () async {
      var result = await Process.start(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'latin', '-s', '29']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.runes.length, 29, reason: 'Expect default size');
    });

    test('Test size --size', () async {
      var result = await Process.start('dart',
          ['run', 'passwd_gen_cli', '-o', '-c', 'latin', '--size', '29']);
      var resultAsString =
          (await result.stdout.transform(utf8.decoder).toList()).first.trim();
      expect(resultAsString.runes.length, 29, reason: 'Expect default size');
    });

    test('Test number -n', () {
      var result = Process.runSync(
          'dart', ['run', 'passwd_gen_cli', '-o', '-c', 'latin', '-n', '10']);
      var resultAsList = result.stdout.toString().trim().split('\n');
      for (var item in resultAsList) {
        expect(item.runes.length, defaultSize, reason: 'Expect default size');
      }
      expect(resultAsList.length, 10, reason: 'Expect length of  10');
    });

    test('Test number --number', () {
      var result = Process.runSync('dart',
          ['run', 'passwd_gen_cli', '-o', '-c', 'latin', '--number', '1000']);
      var resultAsList = result.stdout.toString().trim().split('\n');
      for (var item in resultAsList) {
        expect(item.runes.length, defaultSize, reason: 'Expect default size');
      }
      expect(resultAsList.length, 1000, reason: 'Expect length of  1000');
    });

    test('Test number entropy', () {
      var result = Process.runSync('dart',
          ['run', 'passwd_gen_cli', '-o', '-c', 'latin', '--number', '1000']);
      var resultAsList = result.stdout.toString().trim().split('\n');
      for (var item in resultAsList) {
        expect(item.runes.length, defaultSize, reason: 'Expect default size');
      }
      expect(resultAsList.length, 1000, reason: 'Expect length of  1000');
    });

    test('Test entropy', () {
      var short = Process.runSync('dart', ['run', 'passwd_gen_cli', '-e']);
      var long =
          Process.runSync('dart', ['run', 'passwd_gen_cli', '--entropy']);
      var shortAsString = short.stdout.toString().trim().split('\n').last;
      var longAsString = long.stdout.toString().trim().split('\n').last;
      expect(shortAsString == longAsString, true,
          reason: 'Expect both result are equal.');
      //print(longAsString.substring(longAsString.length-2));
      expect(int.parse(longAsString.substring(longAsString.length - 2)) == 98,
          true);
    });

    test('Test items from file', () {
      var short = Process.runSync('dart',
          ['run', 'passwd_gen_cli', '--file', './test/katakana-items.txt']);
      var long = Process.runSync(
          'dart', ['run', 'passwd_gen_cli', '-f', './test/emoji-items.txt']);
      var shortAsString = short.stdout.toString().trim().split('\n').last;
      var longAsString = long.stdout.toString().trim().split('\n').last;
      //print(shortAsString);
      //print(longAsString);
      expect(shortAsString.runes.length, longAsString.runes.length,
          reason: 'Expect both result are equal.');
      expect(shortAsString.contains(RegExp(katakana)), true,
          reason: 'Expect katakana characters');
      expect(longAsString.contains(RegExp(emoji)), true,
          reason: 'Expect emoji characters');
    });

    test('Test items from 2 files', () {
      var result = Process.runSync('dart', [
        'run',
        'passwd_gen_cli',
        '-f',
        './test/emoji-items.txt',
        '--file',
        './test/katakana-items.txt'
      ]);
      var resultAsString = result.stdout.toString().trim().split('\n').last;
      //print(resultAsString);
      expect(resultAsString.contains(RegExp(katakana)), true,
          reason: 'Expect katakana characters');
      expect(resultAsString.contains(RegExp(emoji)), true,
          reason: 'Expect emoji characters');
    });

    test('Test exclude', () {
      var excludeChar = 'ô,ù,û,ü,ÿ,æ,œ,«,»,’,i,l,1';
      var regex = r'[ôùûüÿæœ«»’il1]';
      var short = Process.runSync('dart', [
        'run',
        'passwd_gen_cli',
        '-o',
        '-c',
        'french',
        '-x',
        excludeChar,
        '-s',
        '100'
      ]);
      var long = Process.runSync('dart', [
        'run',
        'passwd_gen_cli',
        '-o',
        '-c',
        'french',
        '--exclude',
        excludeChar
      ]);
      var shortAsString = short.stdout.toString().trim().split('\n').last;
      var longAsString = long.stdout.toString().trim().split('\n').last;
      //print(shortAsString);
      //print(longAsString);
      expect(shortAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(shortAsString.contains(RegExp(regex)), false,
          reason: 'Expect no french characters');
      expect(shortAsString.runes.length, 100, reason: 'Expect size 100');

      expect(longAsString.contains(RegExp(latin)), true,
          reason: 'Expect latin characters');
      expect(longAsString.contains(RegExp(regex)), false,
          reason: 'Expect no french characters');
      expect(longAsString.runes.length, defaultSize,
          reason: 'Expect default size');
    });
  });

  // To run tests you should start daemon as:
  // dart run passwd_gen_cli --daemon -f "emoji|test/emoji-items.txt"
  group('daemon tests', () {
    setUp(() async {
      print('Test daemon is up');
      try {
        await _httpCall('version');
      } on Exception catch (e) {
        const msg = 'Expect daemon is up. start using: '
            'dart run passwd_gen_cli --daemon -f "emoji|test/emoji-items.txt"';
        print(msg);
        print(e.toString());
        throw Exception(msg);
      }
    });

    tearDown(() async {
      //print('tearDown call');
    });

    test('read index html', () {
      var content = String.fromCharCodes(zlib.decode(base64Decode(indexHtml)));
      final result = content.isNotEmpty &&
          content.contains('<html lang="en">') &&
          content.contains('</html>');
      expect(result, true, reason: 'expect html content.');
    });

    test('read index html', () async {
      await _httpCall('').then((content) => {
            expect(content.contains('<html lang="en">'), true,
                reason: 'expect html content.'),
          });
    });

    test('generate one password length 15', () async {
      final collections = ['latin', 'french', 'italian', 'spanish', 'german'];
      for (var collection in collections) {
        var content = await _httpCall('generate/$collection');
        //print('$content : ${content.trim().runes.length}');
        expect(content.trim().runes.length, 15,
            reason: 'expect content size 15 (default)');
      }
    });

    test('generate one password size 100', () async {
      final collections = ['latin', 'french', 'italian', 'spanish', 'german'];
      for (var collection in collections) {
        var content = await _httpCall('generate/$collection?size=100');
        //print('$content : ${content.trim().runes.length}');
        expect(content.trim().runes.length, 100,
            reason: 'expect content size 100');
      }
    });

    test('generate one password number 10', () async {
      final collections = ['latin', 'french', 'italian', 'spanish', 'german'];
      for (var collection in collections) {
        var content = await _httpCall('generate/$collection?number=10');
        //print('$content : ${content.trim().runes.length}');
        expect(content.split('\n').length, 10,
            reason: 'expect number of passwords = 10');
      }
    });

    test('Test Json', () async {
      final collections = ['latin', 'french', 'italian', 'spanish', 'german'];
      for (var collection in collections) {
        var content = await _httpCall('generate/$collection?number=10&json');
        var arrayOfPasswords = jsonDecode(content);
        expect(arrayOfPasswords.length, 10,
            reason: 'expect number of passwords = 10 as an array');
      }
    });

    test('Test exclude', () async {
      var regex = r'[éàèüöä]';
      var content = await _httpCall(
          'generate/french?exclude=é,à,è,ü,ö,ä&json&number=1000');
      List<dynamic> arrayOfPasswords = jsonDecode(content);
      for (var password in arrayOfPasswords) {
        expect(password.contains(RegExp(regex)), false,
            reason: 'Expect no excluded char');
      }
    });

    test('test version', () async {
      var content = await _httpCall('version');
      expect(content.compareTo(version), 0,
          reason: 'Expect version equal $version');
    });

    test('test custom collection emoji', () async {
      var content = await _httpCall('generate/emoji');
      //print(content);
      expect(content.trim().runes.length, 15, reason: 'expect content size 15');
    });

    test('test httpClient', () async {
      var httpClient = HttpClient();
      var sb = StringBuffer();
      var response = await httpClient
          .get('localhost', 4040, 'generate/french')
          .then((request) => request.close())
          .whenComplete(() => print('done'));
      await for (var contents in response.transform(Utf8Decoder())) {
        sb.write(contents);
      }
      print(sb.length);
      print(sb.toString());
    }, skip: 'Debugging HttpClient purpose.');
  });
}

Future<String> _httpCall(String path) async {
  var httpClient = HttpClient();
  var sb = StringBuffer();
  var response = await httpClient
      .get('localhost', 4040, path)
      .then((request) => request.close())
      .whenComplete(() => print('Request $path done'));
  await for (var contents in response.transform(Utf8Decoder())) {
    sb.write(contents);
  }
  expect(response.statusCode, HttpStatus.ok, reason: 'expect status code OK');
  return Future.value(sb.toString());
}
