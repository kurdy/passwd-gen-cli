/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE file.
 */
// ignore_for_file: lines_longer_than_80_chars
/// OPTIONS text
const options = 'OPTIONS:\n'
    '\t-p, --port\t\tport number > 1024, default=4040\n'
    '\t-f, --file\t\tText file with items (one item per line)\n'
    '\t\t\t\t\tIt can be file or key|file. Examples: -f ./path/foo.txt or -f mywords|./path/foo.txt\n'
    '\t-i, --index\t\tAllows to provide an alternative version of the HTML content.\n';

/// Usage text
const usage = 'USAGE:\n'
    '\tdaemon start a basic web server. With UI on http://localhost:{port}/\n'
    '\tpasswd_gen_cli [--daemon | -d] [--port | -p]\n';

/// Client HTTP request
const requests = 'REQUESTS:\n'
    '\thttp://localhost:{port}/generate/{collection}?parameters\n'
    '\t\tcollection\t: default = latin\n'
    '\t\tsize\t\t: default = 15\n'
    '\t\tnumber\t\t: default = 1\n'
    '\t\tjson\t\t: otherwise text\n'
    '\t\texclude\t\t: as string, items are separated by comma\n'
    '\thttp://localhost:{port}/license ➜ show license.\n'
    '\thttp://localhost:{port}/version ➜ show application version.\n'
    '\thttp://localhost:{port}/collections ➜ json with id\'s of custom items\n'
    '\thttp://localhost:{port}/stop ➜ Stop daemon.\n'
    '\thttp://localhost:{port} ➜ show web UI.\n'
    '\tExamples:\n'
    '\t\thttp://localhost:4040/generate/french?size=29&number=10&json\n'
    '\t\thttp://localhost:4040/stop ➜ Stop daemon.\n';
