/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE file.
 */

import 'dart:convert';
import 'dart:io';

import 'package:passwd_gen/passwd_gen.dart';

import '../passwd_gen_cli.dart' as cli;
import '../texts_en.dart' as cli_text;
import 'index.dart';
import 'texts_en.dart' as daemon;

/// Default port number
const defaultPortNumber = 4040;

final _customCollectionItems = <String, List<String>>{};
late String? _customIndexHtml;

/// Start the standalone server,
Future<int> launchDaemon(List<String> arguments) async {
  var exitCode = cli.exitNok;
  final flags = cli.extractFlags(arguments);
  if (flags.contains(cli.Flags.help)) {
    _showHelp();
    return cli.exitOk;
  }
  final port = cli.extractOption('-p', '--port', arguments);
  final portNumber = port == null ? defaultPortNumber : int.tryParse(port);
  if (portNumber == null || portNumber < 1024) {
    print('port number invalid {$port,$portNumber}. It should be > 1024');
    return cli.exitNok;
  }
  final collections = cli.getItemsFromFile(arguments);
  if (collections != null && collections.isNotEmpty) {
    _customCollectionItems.addAll(collections);
    //print('${_customCollectionItems.keys}');
  }

  final index = cli.extractOption('-i', '--index', arguments);
  _customIndexHtml = index == null ? null : await File(index).readAsString();

  exitCode = await _startServer(portNumber);
  print('Server Stopped !');

  return exitCode;
}

Future<int> _startServer(int port) async {
  var code = cli.exitNok;
  print('Starting server ....');
  var server = await HttpServer.bind(
    InternetAddress.loopbackIPv4,
    port,
  );
  print('Server started waiting a request. '
      '${InternetAddress.loopbackIPv4} port $port');
  print('Open url : http://${InternetAddress.loopbackIPv4.host}:$port');

  await for (var request in server) {
    //request.response.headers.add("Access-Control-Allow-Origin", "*");
    if (request.uri.pathSegments.isNotEmpty &&
        request.uri.pathSegments.first.toLowerCase() == 'stop') {
      print('Stopping server ....');

      request.response
        ..statusCode = HttpStatus.gone
        ..headers.set('Cache-Control', 'no-cache')
        ..write('Stopping server ....');
      await request.response.close();
      code = cli.exitOk;
      break;
    } else {
      _handleRequest(request);
    }
  }
  return Future.value(code);
}

PasswordService _createService(Uri uri) {
  late final PasswordService generator;
  final collectionName = uri.pathSegments.last.toLowerCase();
  switch (collectionName) {
    case 'latin':
      generator = PasswordService.latinBase();
      break;
    case 'french':
      generator = PasswordService.latinFrench();
      break;
    case 'german':
      generator = PasswordService.latinGerman();
      break;
    case 'spanish':
      generator = PasswordService.latinSpanish();
      break;
    case 'italian':
      generator = PasswordService.latinItalian();
      break;
    case 'eff':
      generator = PasswordService.effLargeListWords();
      break;
    default:
      if (_customCollectionItems.isNotEmpty) {
        generator = PasswordService();
        for (var key in _customCollectionItems.keys) {
          if (key.toLowerCase() == collectionName) {
            generator.addCollectionOfPasswordItems(
                key, _customCollectionItems[key]!);
          }
        }
      }
      if (generator.getCollectionKeys().isEmpty) {
        generator = PasswordService.latinBase();
      }
  }

  return generator;
}

int _stringAsInt(String? value, {defaultValue = 0}) {
  int result = defaultValue;
  if (value != null) {
    final parsed = int.tryParse(value);
    result = parsed ?? defaultValue;
  }
  return result;
}

void _doCollections(HttpRequest request) {
  request.response
    ..statusCode = HttpStatus.ok
    ..headers.set('Cache-Control', 'no-cache')
    ..headers.contentType = ContentType.json
    ..write(jsonEncode(_customCollectionItems.keys.toList(growable: false)))
    ..close();
}

void _doIndexHtml(HttpRequest request) {
  request.response
    ..statusCode = HttpStatus.ok
    ..headers.set('Cache-Control', 'no-cache')
    ..headers.contentType = ContentType.html
    ..write(_customIndexHtml ??
        String.fromCharCodes(zlib.decode(base64Decode(indexHtml))))
    ..close();
}

void _doLicense(HttpRequest request) {
  request.response
    ..statusCode = HttpStatus.ok
    ..headers.set('Cache-Control', 'no-cache')
    ..headers.contentType = ContentType.text
    ..write(cli_text.license)
    ..close();
}

void _doVersion(HttpRequest request) {
  request.response
    ..statusCode = HttpStatus.ok
    ..headers.set('Cache-Control', 'no-cache')
    ..headers.contentType = ContentType.text
    ..write(cli_text.version)
    ..close();
}

void _doPasswords(HttpRequest request, PasswordService generator, int size,
    int number, String? separator, String? exclude,
    [isTypeText = true]) {
  var passwords = <String>[];
  for (var i = 0; i < number; i++) {
    final excludeAsList = exclude?.split(',').toList(growable: false);
    final p =
        generator.generatePassword(length: size, excludeItems: excludeAsList);
    if (separator == null && p.password.first.runes.length > 1) {
      p.separator = ' ';
    } else if (separator != null) {
      p.separator = separator;
    }
    passwords.add(p.toString());
  }
  request.response
    ..statusCode = HttpStatus.ok
    ..headers.set('Cache-Control', 'no-cache')
    ..headers.contentType = isTypeText ? ContentType.text : ContentType.json
    ..write(isTypeText
        ? passwords.reduce((value, element) => '$value\n$element')
        : jsonEncode(passwords))
    ..close();
}

void _handleRequest(HttpRequest request) {
  ///print ('request ${request.uri}');
  try {
    if (request.method == 'GET') {
      var parameters = <String, String>{};
      // Convert to a lowercase parameters Map
      request.uri.queryParameters.forEach((key, value) =>
          parameters.putIfAbsent(
              key.trim().toLowerCase(), () => value.trim().toLowerCase()));
      if (request.uri.pathSegments.isNotEmpty) {
        if (request.uri.pathSegments.first.toLowerCase() == 'generate') {
          _doPasswords(
              request,
              _createService(request.uri),
              _stringAsInt(parameters['size'], defaultValue: 15),
              _stringAsInt(parameters['number'], defaultValue: 1),
              parameters['separator'],
              parameters['exclude'],
              parameters['json'] == null);
        } else if (request.uri.pathSegments.first.toLowerCase() ==
            'collections') {
          _doCollections(request);
        } else if (request.uri.pathSegments.first.toLowerCase() == 'license') {
          _doLicense(request);
        } else if (request.uri.pathSegments.first.toLowerCase() == 'version') {
          _doVersion(request);
        } else {
          _doIndexHtml(request);
        }
      } else {
        _doIndexHtml(request);
      }
    } else {
      print('Unsupported request: ${request.method}.');
      request.response
        ..statusCode = HttpStatus.methodNotAllowed
        ..write('Unsupported request: ${request.method}.')
        ..close();
    }
  } on Exception catch (e) {
    print('Exception in handleRequest: $e');
    request.response
      ..statusCode = HttpStatus.serviceUnavailable
      ..write('Server exception: $e.')
      ..close();
    // ignore: avoid_catches_without_on_clauses
  } catch (e) {
    print('Exception in handleRequest: $e');
    request.response
      ..statusCode = HttpStatus.serviceUnavailable
      ..write('Server exception: $e.')
      ..close();
    print('Request handled.');
  }
}

//Show application help
void _showHelp() {
  print(daemon.usage);
  print(daemon.options);
  print(daemon.requests);
}
