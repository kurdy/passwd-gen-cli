/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE file.
 */

import 'dart:io';

import 'package:ansicolor/ansicolor.dart';
import 'package:passwd_gen/passwd_gen.dart';

import 'texts_en.dart' as cli;

// Create a non break space
String _noBreakSpace() => String.fromCharCode(0x00A0);

/// Exit code successfully value 0
const exitOk = 0;

/// Exit code failure value 1
const exitNok = 1;

/// Password default size
const defaultSize = 15;

/// Show application help
void showHelp() {
  print(cli.appDescription);
  print(cli.usage);
  print(cli.flags);
  print(cli.options);
  print(cli.exitStatus);
}

/// Show application version
void showVersion() {
  print('${cli.appName} ${cli.version}');
}

/// Show application license
void showLicense() {
  print(cli.license);
}

/// Create and display on terminal one or more password or passphrase.
/// [arguments] are arguments from command line
/// [displayEntropy] Allow to display bits of entropy
/// [useColor] Allow to to use [AnsiPen] to use color on terminal
int makePassword(List<String> arguments,
    {bool displayEntropy = false, bool useColor = true}) {
  var collection = getCollection(arguments);
  var size = getSize(arguments);
  var number = getNumber(arguments);
  var exclude = getExclude(arguments);
  var itemsFromFile = getItemsFromFile(arguments);

  late PasswordService generator;
  switch (collection) {
    case Collection.latin:
      generator = PasswordService.latinBase();
      break;
    case Collection.french:
      generator = PasswordService.latinFrench();
      break;
    case Collection.german:
      generator = PasswordService.latinGerman();
      break;
    case Collection.italian:
      generator = PasswordService.latinItalian();
      break;
    case Collection.spanish:
      generator = PasswordService.latinSpanish();
      break;
    case Collection.eff:
      generator = PasswordService.effLargeListWords();
      break;
    default:
      // If cli option -f isn't used. It set by default latinBase
      generator = itemsFromFile != null && itemsFromFile.isNotEmpty
          ? PasswordService()
          : PasswordService.latinBase();
  }

  // Test if cli option -f. If yes read from file custom collection
  var hasCustomItems = itemsFromFile != null && itemsFromFile.isNotEmpty;
  if (hasCustomItems) {
    itemsFromFile.forEach((key, collection) =>
        generator.addCollectionOfPasswordItems(key, collection));
  }

  // Create and display password,passphrase
  late Password password;
  for (var i = 0; i < number; i++) {
    password = generator.generatePassword(length: size, excludeItems: exclude);
    displayPassword(password, useColor: !hasCustomItems && useColor);
  }
  if (displayEntropy) {
    print('\nBits of entropy ${password.entropy.toInt()}');
  }

  return exitOk;
}

/// Display on terminal result of [makePassword]
void displayPassword(Password password, {bool useColor = true}) {
  ansiColorDisabled = false;
  final sb = StringBuffer();
  final pen = AnsiPen();
  //print(password.toString());

  // Without color just print password
  if (!useColor) {
    if (password.password.first.runes.length > 1) {
      password.separator = _noBreakSpace();
    }
    print(password.toString());
    return;
  }

  // With color setup color and print
  for (var items in password.password) {
    // Use runes.length instead of length otherwise some char length
    // can be egal to 2.
    // the test distinguishes the password and passphrase
    if (items.runes.length > 1) {
      sb.write(_noBreakSpace());
      pen.green();
      sb.write(pen.write(items));
    } else {
      if (items.contains(RegExp(r'[0-9]'))) {
        pen.blue();
        sb.write(pen.write(items));
      } else if (items.contains(RegExp(r'[a-z]'))) {
        pen.white();
        sb.write(pen.write(items));
      } else if (items.contains(RegExp(r'[A-Z]'))) {
        pen.white(bold: true);
        sb.write(pen.write(items));
      } else {
        pen.red();
        sb.write(pen.write(items));
      }
    }
  }
  print(sb);
}

/// Enumerate embedded collection
// ignore: public_member_api_docs
enum Collection { latin, french, german, italian, spanish, eff, none }

/// Get the collection as [Collection] from cli arguments
Collection? getCollection(List<String> arguments) {
  var option = extractOption('-c', '--collection', arguments);
  if (option != null && option.isNotEmpty) {
    switch (option.toLowerCase()) {
      case 'latin':
        return Collection.latin;
      case 'french':
        return Collection.french;
      case 'german':
        return Collection.german;
      case 'italian':
        return Collection.italian;
      case 'spanish':
        return Collection.spanish;
      case 'eff':
        return Collection.eff;
      default:
        return Collection.none;
    }
  }
  return Collection.none;
}

/// Get size as [int] from cli arguments
int getSize(List<String> arguments) {
  var option = extractOption('-s', '--size', arguments);
  if (option != null && option.isNotEmpty) {
    var size = int.tryParse(option);
    if (size != null) {
      return size;
    }
  }
  return defaultSize;
}

/// Get number of password/passphrase to generate as [int] from cli arguments
int getNumber(List<String> arguments) {
  const defaultNumber = 1;
  var option = extractOption('-n', '--number', arguments);
  if (option != null && option.isNotEmpty) {
    var number = int.tryParse(option);
    if (number != null) {
      return number;
    }
  }
  return defaultNumber;
}

/// Get list of items to exclude as [List] from cli arguments
List<String>? getExclude(List<String> arguments) {
  var option = extractOption('-x', '--exclude', arguments);
  if (option != null && option.isNotEmpty) {
    return option.split(',').toList(growable: false);
  }
  return null;
}

/// Get custom items from file as [Map] from cli arguments
Map<String, List<String>>? getItemsFromFile(List<String> arguments) {
  var result = <String, List<String>>{};
  for (var i = 0; i < arguments.length; i++) {
    if (arguments.elementAt(i).toLowerCase() == '-f' ||
        arguments.elementAt(i).toLowerCase() == '--file') {
      // file argument can be a path/file or key|path/file or key|path/file|name
      var fileArgument = arguments.elementAt(i + 1);
      var key = fileArgument;
      final index = fileArgument.indexOf('|');
      if (index > 0) {
        key = fileArgument.substring(0, index);
        fileArgument = fileArgument.substring(index + 1, fileArgument.length);
      }
      //print(key);
      var file = File(fileArgument);
      result.putIfAbsent(
          key, () => file.readAsLinesSync().toList(growable: false));
      i++;
    }
  }
  return result;
}

/// Helper to extract option from cli
String? extractOption(String short, String long, List<String> arguments) {
  String? value;
  if (arguments.contains(short)) {
    value = arguments.elementAt(arguments.indexOf(short) + 1);
  } else if (arguments.contains(long)) {
    value = arguments.elementAt(arguments.indexOf(long) + 1);
  }
  return value;
}

/// Enumerate flags from cli
// ignore: public_member_api_docs
enum Flags { help, license, version, daemon, entropy, noColor, none }

/// Helper to extract flags from cli
List<Flags> extractFlags(List<String> arguments) {
  var flags = <Flags>[];
  var args = arguments.map((e) => e.toLowerCase());

  if (args.contains('-o') || args.contains('--noColor')) {
    flags.add(Flags.noColor);
  }

  if (args.contains('-e') || args.contains('--entropy')) {
    flags.add(Flags.entropy);
  }

  if (args.contains('-d') || args.contains('--daemon')) {
    flags.add(Flags.daemon);
  }

  if (args.contains('-h') || args.contains('--help')) {
    flags.add(Flags.help);
  } else if (args.contains('-l') || args.contains('--license')) {
    flags.add(Flags.license);
  } else if (args.contains('-v') || args.contains('--version')) {
    flags.add(Flags.version);
  } else {
    flags.add(Flags.none);
  }
  return flags;
}
