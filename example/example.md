# Examples

## Show version
```shell
passwd_gen -v
```

### Result: 
```text
passwd_gen_cli 1.0.2
```

## Get help
```shell
passwd_gen_cli -h
```

### Result:

```text
Application to generate passwords or a phrase composed of several words.
It's possible to use one or more collections of items as source to create them.

USAGE:
        passwd_gen_cli [FLAGS] [OPTIONS]...
        passwd_gen_cli                  Will create one password with default parameters

FLAGS:
        -d, --daemon                    Start a long-running daemon process.
                                                Use -h, --help with daemon to learn more about http daemon server.
        -e, --entropy                   Display entropy.
        -o, --noColor                   Don't use color to display result.
        -h, --help                      Print a help message
        -l, --license                   Prints license information
        -v, --version                   Prints version information

OPTIONS:
        -c, --collection                latin or french or german or spanish or italian or eff
        -s, --size                      password size default=15
        -n, --number                    number of password to generate default=1
        -f, --file                      Text file with items (one item per line)
        -x, --exclude                   List of excluded items separated by ,

EXIT STATUS:
        The CLI will exit with one of the following values:
        0                               Successful execution.
        1                               Failed executions.
```

```shell
passwd_gen_cli --daemon -h
```

### Result:
```text
USAGE:
        daemon start a basic web server. With UI on http://localhost:{port}/
        passwd_gen_cli [--daemon | -d] [--port | -p]

OPTIONS:
        -p, --port              port number > 1024, default=4040
        -f, --file              Text file with items (one item per line)
                                        It can be file or key|file. Examples: -f ./path/foo.txt or -f mywords|./path/foo.txt
        -i, --index             Allows to provide an alternative version of the HTML content.

REQUESTS:
        http://localhost:{port}/generate/{collection}?parameters
                collection      : default = latin
                size            : default = 15
                number          : default = 1
                json            : otherwise text
                exclude         : as string, items are separated by comma
        http://localhost:{port}/license ➜ show license.
        http://localhost:{port}/version ➜ show application version.
        http://localhost:{port}/collections ➜ json with id's of custom items
        http://localhost:{port}/stop ➜ Stop daemon.
        http://localhost:{port} ➜ show web UI.
        Examples:
                http://localhost:4040/generate/french?size=29&number=10&json
                http://localhost:4040/stop ➜ Stop daemon.
```


## Create a password using latin collection and size of 15 characters

```shell
passwd_gen
```

These commands are equivalent because these are the default settings.

```shell
passwd_gen --collection latin --size 15
```

### Result:
```text
Yi72ehayU[{4|Yg
```

## Using different collections

```shell
passwd_gen --collection french;

passwd_gen -c german;

passwd_gen -c italian;
```

### Result:
```text
1wZLM1j+ùï9>èw?
```

### Create passphrase based on [EFF's](https://www.eff.org/dice) words

```shell
passwd_gen --collection eff;
```

#### Result:
```text
marbling anemia tartar lifter opal elevation underhand vengeful exemplary guts blooper vicinity improvise sheet amber
```

### Create passphrase based on custom file

#### Passphrase
```shell
passwd_gen --file ./test/eduscol_fr.txt;
```

#### Result:
```text
liberté mouvement famille combat descendre presque rencontrer serrer forêt principe société enfance troubler promener prononcer
```
#### password

```shell
passwd_gen --file ./test/katakana-items.txt;
```

#### Result:
```text
ゲン・ヘヅポヲジヶクワグヌユギ
```

#### From 2 files

```shell
passwd_gen --file ./test/katakana-items.txt --file ./test/emoji-items.txt;
```

#### Result:
```text
😨ル🙇ュチピ😖🙊ォ😎😯😆ウ😕ブ
```

## Generate multiple passwords 

```shell
passwd_gen --number 10;
```

### Result:
```text
X,@ej@9K16qpjrQ
6JgjvFc\\4n]Wf9
5daX£6jo=Kd'5Vj
6"dU#zT3oy6Mq!k
w€uZD3zt9[K:7si
0d-gu2XTZb$$b8p
Id1|srP'5h,8dhL
sc?Y9j,sDi'k49P
v4,rXuh:l8NT~i6
h+f0m5d+-IyXu5J
```

## Generate password with a size of 25 and show bits of entropy value

```shell
passwd_gen --size 25 -e
```

### Result:
```text
YfveD4396"[WL6F0_&p-\qGgj

Bits of entropy 164
```

## Exclude some characters (size=29)

```shell
passwd_gen -c french --exclude 'ô,ù,û,ü,ÿ,æ,œ,«,»,’,i,l,1' -s 29;
```

### Result:

```text
d8véB+î5xë7m'cz|ao8*YWR9êî£eA
```

## Use daemon

```shell
passwd_gen --daemon -f "emoji|test/emoji-items.txt";
```

### Result:

```text
Starting server ....
Server started waiting a request. InternetAddress('127.0.0.1', IPv4) port 4040
Open url : http://127.0.0.1:4040
```
default url [http://127.0.0.1:4040](http://127.0.0.1:4040)
