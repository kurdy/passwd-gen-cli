## 0.0.1-dev

- Initial version. (not published)

##  1.0.0-beta.1

- First published version

##  1.0.0-rc.1

- First release candidate

##  1.0.0-rc.2

- Code comment line to avoid  "Access-Control-Allow-Origin", "*")
- Correct pedantic issues

##  1.0.0

- Migrate repo to gitlab
- Migrate from pedantic to lints
- Refactor some lints
- Upgrade dependencies & Tests

##  1.0.1

- No functional upgrade

##  1.0.2

- No functional (pub.dev score issues)
