/*
 * Copyright (c) 2021.  ʕʘ̅͜ʘ̅ʔ
 * Use of this source code is governed by a
 * MIT license that can be found in the LICENSE file.
 */

import 'dart:io';

import 'package:ansicolor/ansicolor.dart';
import 'package:passwd_gen_cli/daemon/passwd_gen_http.dart' as http;
import 'package:passwd_gen_cli/passwd_gen_cli.dart' as cli;

/// Entry point
/// ```text
/// 	Application to generate passwords or a phrase composed of several words.
// ignore: lines_longer_than_80_chars
/// 	It's possible to use one or more collections of items as source to create them.
///
/// USAGE:
/// 	passwd_gen_cli [FLAGS] [OPTIONS]...
/// 	passwd_gen_cli			Will create one password with default parameters
///
/// FLAGS:
/// 	-d, --daemon			Start a long-running daemon process.
/// 	-e, --entropy			Display entropy.
/// 	-o, --noColor			Don't use color to display result.
/// 	-h, --help				Print a help message
/// 	-l, --license			Prints license information
/// 	-v, --version			Prints version information
///
/// OPTIONS:
/// 	-c, --collection		latin or french or german or spanish or italian or eff
/// 	-s, --size				password size default=15
/// 	-n, --number			number of password to generate default=1
/// 	-f, --file				Text file with items (one item per line)
/// 	-x, --exclude			List of excluded items separated by ,
///
/// EXIT STATUS:
/// 	The CLI will exit with one of the following values:
/// 	0				Successful execution.
/// 	1				Failed executions.
/// ```
void main(List<String> arguments) async {
  //showArguments(arguments);
  var exitCode = cli.exitNok;
  var flags = cli.extractFlags(arguments);
  var displayEntropy = flags.contains(cli.Flags.entropy);
  var useColor = !flags.contains(cli.Flags.noColor);
  if (flags.contains(cli.Flags.daemon)) {
    exitCode = await http.launchDaemon(arguments);
  } else if (flags.contains(cli.Flags.help)) {
    cli.showHelp();
    exitCode = cli.exitOk;
  } else if (flags.contains(cli.Flags.version)) {
    cli.showVersion();
    exitCode = cli.exitOk;
  } else if (flags.contains(cli.Flags.license)) {
    cli.showLicense();
    exitCode = cli.exitOk;
  } else {
    try {
      exitCode = cli.makePassword(arguments,
          displayEntropy: displayEntropy, useColor: useColor);
      exitCode = cli.exitOk;
    } on Exception catch (e) {
      ansiColorDisabled = false;
      final pen = AnsiPen();
      pen.red(bold: true);
      print(pen.write('\t${String.fromCharCode(0x1F92F)}\t${e.toString()}'));
      exitCode = cli.exitNok;
    }
  }

  exit(exitCode);
}

// Helper function to print arguments given to the command
void showArguments(List<String> arguments) {
  var i = 0;
  for (var item in arguments) {
    print('${++i} $item');
  }
}
